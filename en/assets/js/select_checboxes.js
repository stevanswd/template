$( document ).ready(function() {

	$('.multiselect').click(function() {
        if( $(this).hasClass('cs-active') ) {
            $(this).next('.select_checkboxes').hide();
            $(this).removeClass('cs-active');
        }
        else {
            $(this).next('.select_checkboxes').show();
            $(this).addClass('cs-active');
        }
	});

});