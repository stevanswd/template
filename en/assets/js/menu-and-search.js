$( document ).ready(function() {

	// Filtros project at glance
	$('.container-countries-at-glance .filters .filter_btn').click(function(e){
		e.preventDefault();
		$(this).toggleClass('active');
		if( $(this).hasClass('active') ) {
			$('.container-countries-at-glance .filters input').show();
		}else{
			$('.container-countries-at-glance .filters input').hide();
		}
	});

	// Selects
	if( !$('body').hasClass('InternetExplorer') ) {
		// Change select styles
		[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
			new SelectFx(el);
		});
	}

	searchActions();
	function searchActions() {
		$('#btn_search_mobile').click(function() {
			toggleSearch();
			var scrollTo = 0;
			if( $(this).hasClass('active') ) {
				scrollTo = $(".content-bar-top").offset().top - 50;
			}
			$('html, body').animate({ scrollTop: scrollTo }, 500);
		});

		$('#btn_search').click(function() {
			toggleSearch();
			if( $(this).hasClass('active') ) {
				$('html, body').animate({ scrollTop: 0 }, 500);
			}
		});

		$('.content-bar-top .icon-icon-closewindw ').click(function(){
			toggleSearch();
			if( $('#top').is(":visible") ) {
				$('html, body').animate({ scrollTop: 0 }, 500);
			}
		});
	}

	function toggleSearch() {
		$('#btn_search_mobile').toggleClass("active");
		$('#btn_search').toggleClass("active");
		$('.content-bar-top').slideToggle();
		$('.content-bar-top').toggleClass("active");
	}

	menuMobile();
	function menuMobile() {
		$('#slide-nav.navbar .container').append($('<div id="navbar-height-col"></div>'));
    
	    var toggler = '.navbar-toggle';
	    var pagewrapper = '#page-content';
	    var navigationwrapper = '.navbar-header';
	    var menuwidth = '100%'; 
	    var slidewidth = '80%';
	    var menuneg = '-100%';
	    var slideneg = '-80%';

	    $("#slide-nav").on("click", toggler, function (e) { 
	        var selected = $(this).hasClass('slide-active');

	        $('#nav-main').stop().animate({
	            left: selected ? menuneg : '0px'
	        });

	        $('#navbar-height-col').stop().animate({
	            left: selected ? slideneg : '0px'
	        });

	        $(navigationwrapper).stop().animate({
	            left: selected ? '0px' : slidewidth
	        });

	        $(this).toggleClass('slide-active', !selected);
	        $('#nav-main').toggleClass('slide-active');

	        $('#page-content, .navbar, body, .navbar-header').toggleClass('slide-active');  

	        if( $('#page-content, .navbar, body, .navbar-header').hasClass('slide-active') ) {
	        	$('html, body').animate({ scrollTop: 0 }, 500);
	        }
	    });

	    var selected = '#nav-main, #page-content, body, .navbar, .navbar-header';

	    $(window).on("resize", function () {
	        if ($(window).width() > 767 && $('.navbar-toggle').is(':hidden')) {
	            $(selected).removeClass('slide-active');
	        }
	    });
	}
});