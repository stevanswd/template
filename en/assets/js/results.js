$( document ).ready(function() {

	$('.container_results .refine .row h3').click(function() {
		$(this).find('.icon').toggleClass('icon-icon-filter-collapse').toggleClass('icon-icon-filter-open');
		$(this).parent('.row').find('.row-content').slideToggle();
	});

	$('.container_results .refine .row .row-content ul li p').click(function() {
		$(this).find('.icon').toggleClass('icon-icon-filter-collapse').toggleClass('icon-icon-filter-open');
		$(this).next('ul').slideToggle();
	});

});